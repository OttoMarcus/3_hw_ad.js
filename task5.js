const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  //------------------------------------------- condition end ---------------------------------------------

//basic logic
  const bookResult = [...books, bookToAdd];
//Task header counter
let counter5 = makeCounter();
//Structure generator
addTask(counter5());
//Task entry point
const entryPoint5 = document.querySelector(".task5" );
//Run logic function
arrayOfObjects(bookResult, entryPoint5);