const container = document.querySelector(".container");

//Counter
let currentCount = 1;
function makeCounter() {
    return function() {
        return   currentCount++;
    };
}

//Structure generator function
function addTask(counter) {
    let article = document.createElement("div");
    article.classList.add("article");
    container.appendChild(article);

    let b = document.createElement("b");
    b.innerText = "Завдання - " + counter;
    article.appendChild(b);

    let ul = document.createElement("ul");
    ul.classList.add("task" + counter);
    article.appendChild(ul);
}

// Show items of Array
function itemsOfArray(arr, entryPoint) {
    for (let obj of arr) {
        let listItem = document.createElement("li");
        listItem.classList.add("description");
        listItem.innerHTML = obj;
        entryPoint.appendChild(listItem);
    }
}

// Show array of objects
function arrayOfObjects(arr, entryPoint) {
    for (let obj of arr) {
      let listItem = document.createElement("li");
      listItem.classList.add("description");
      listItem.innerHTML = [...Object.values(obj)].join("  -  ");
      entryPoint.appendChild(listItem);
    }
}


//List items from nasted objects
function objectsInObjects(obj, entryPoint) {
    const ulElement = document.createElement('ul');
    
    for (let key in obj) {
        let listItem = document.createElement("li");
        listItem.classList.add("description");
        listItem.innerHTML = `${key}:`;

        if (typeof obj[key] === 'object') {
            objectsInObjects(obj[key], listItem);
        } else {
            const spanElement = document.createElement('span');
            spanElement.innerHTML = obj[key];
            listItem.appendChild(spanElement);
        }

        ulElement.appendChild(listItem);
    }

    entryPoint.appendChild(ulElement);
}

