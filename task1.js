const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

//------------------------------------------- condition end ---------------------------------------------


//basic logic
const clientSum = [...new Set([...clients1, ...clients2])];
//Task header counter
let counter1 = makeCounter();
//Structure generator
addTask(counter1());
//Task entry point
const entryPoint1 = document.querySelector(".task1" );
//Run logic function
itemsOfArray(clientSum, entryPoint1);