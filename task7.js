const array = ['value', () => 'showValue'];
// const [first] = array;
// const [, showValue] = array;



// alert(first); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'
//------------------------------------------- condition end ---------------------------------------------

const [first, showValue] = array
//Task header counter
let counter7 = makeCounter();
//Structure generator
addTask(counter7());
//Task entry point
const entryPoint7 = document.querySelector(".task7" );
//Run logic function
function addButton(entryPoint) {
    let btn = document.createElement("button");
    btn.classList.add("btn-style");
    btn.innerText = "RESULT";
    entryPoint.appendChild(btn);
}

addButton(entryPoint7);
//call alert function by click listener
const btn = document.querySelector(".btn-style");
btn.addEventListener("click", () => {
    alert(first);
    alert(showValue());
});