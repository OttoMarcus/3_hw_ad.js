const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

  //------------------------------------------- condition end ---------------------------------------------

//basic logic
const employee1 = {...employee, age: 52, salary: 100000}
//Convert object -> array
const arr = [...Object.entries(employee1)];

//Task header counter
let counter6 = makeCounter();
//Structure generator
addTask(counter6());
//Task entry point
const entryPoint6 = document.querySelector(".task6" );
//Run logic function
arrayOfObjects(arr, entryPoint6);