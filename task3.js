const user1 = {
    name: "John",
    years: 30
};

//------------------------------------------- condition end ---------------------------------------------

//basic logic
const {name: імя, years: вік, isAdmin = false} = user1;
//Task header counter
let counter3 = makeCounter();
//Structure generator
addTask(counter3());
//Task entry point
const entryPoint3 = document.querySelector(".task3" );
//Display function
itemsOfArray([імя,  вік, isAdmin], entryPoint3);